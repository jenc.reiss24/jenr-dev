-- Add up migration script here
DROP TABLE IF EXISTS POSTS;
CREATE TABLE POSTS (
    ID          SERIAL          NOT NULL        PRIMARY KEY,
    TITLE       VARCHAR (255)   NOT NULL,
    BODY        TEXT            NOT NULL,
    AUTHOR      VARCHAR (255)   NOT NULL        UNIQUE,
    CREATED_AT  TIMESTAMP       NOT NULL        DEFAULT NOW()
);