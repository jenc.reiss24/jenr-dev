-- Add up migration script here
CREATE TYPE OP_LEVEL AS ENUM ('commenter', 'poster', 'admin');
ALTER TABLE USERS
ADD COLUMN OP_LEVEL OP_LEVEL DEFAULT 'commenter';