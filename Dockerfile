FROM nixos/nix

ARG PG_HOST="localhost"
ARG PG_USER="postgres"
ARG PG_PASSWORD="postgres"
ARG PG_PORT=5432
ARG APP_KEY="password1"
ARG APP_HOST="0.0.0.0"
ARG APP_PORT=50051

RUN nix-channel --update

RUN mkdir /rpc-server
COPY ./. /rpc-server
RUN nix-build rpc-server/package.nix

ENV PG_HOST ${PG_HOST}
ENV PG_USER ${PG_USER}
ENV PG_PASSWORD ${PG_PASSWORD}
ENV PG_PORT ${PG_PORT}
ENV APP_KEY ${APP_KEY}
ENV APP_HOST ${APP_HOST}
ENV APP_PORT ${APP_PORT}

WORKDIR /
EXPOSE 50051
CMD /result/bin/rpc-server \
    --pg-port ${PG_PORT} \
    --pg-user ${PG_USER} \
    --pg-password ${PG_PASSWORD} \
    --pg-host ${PG_HOST} \
    --app-key ${APP_KEY} \
    --app-host ${APP_HOST} \
    --app-port ${APP_PORT}