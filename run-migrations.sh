#!/bin/bash

set -e
set -u

export DATABASE_URL="postgres://${PG_USER}:${PG_PASSWORD}@${PG_HOST}:${PG_PORT}"

nix-shell /shell.nix --command \
    "cargo sqlx migrate run"