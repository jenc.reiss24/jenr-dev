package:
	nix-build package.nix

image:
	nix-shell --command \
		'docker build -t jenr-blog:latest .'