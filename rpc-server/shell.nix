let
  dependencies =
    import
    ./dependencies.nix {};
in
  dependencies.pkgs.mkShell {
    packages = dependencies.developmentDependencies;

    shellHook = ''
      echo "Entering nix shell for blog-rpc..."
    '';
  }
