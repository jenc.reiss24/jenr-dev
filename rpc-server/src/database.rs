use std::sync::Arc;

use sqlx::{postgres::PgPoolOptions, types::time::PrimitiveDateTime};
use tracing::{debug, trace};

use crate::{
    posts::post_api::{Post, PostId, PostUpdate},
    Args, MAX_CONNECTIONS, OpLevel,
};

#[tracing::instrument]
pub async fn connect(args: &Args) -> sqlx::Result<Arc<sqlx::PgPool>> {
    let pg_conn_str = format!(
        "postgres://{}:{}@{}:{}",
        args.pg_user, args.pg_password, args.pg_host, args.pg_port
    );
    debug!("connecting to postgresql database with connection string {}", &pg_conn_str);

    let raw_pool = PgPoolOptions::new()
        .max_connections(MAX_CONNECTIONS)
        .connect(&pg_conn_str)
        .await?;
    trace!("running migrations from /migrations...");
    sqlx::migrate!()
        .run(&raw_pool)
        .await?;

    Ok(Arc::new(raw_pool))
}

use sqlx::Postgres;
pub async fn create_user(
    pg_pool: &sqlx::Pool<Postgres>,
    email: impl AsRef<str>,
    password_hash: impl AsRef<str>,
    username: impl AsRef<str>,
) -> std::result::Result<i32, sqlx::Error> {
    debug!("fetching user from database with email: {}, password_hash: {}, username: {}",
        email.as_ref(),
        password_hash.as_ref(),
        username.as_ref(),
    );
    let record = sqlx::query!(
        r#"
INSERT INTO USERS ( EMAIL, PASSWORD, USERNAME )
VALUES ( $1, $2, $3 )
RETURNING ID
        "#,
        email.as_ref(),
        password_hash.as_ref(),
        username.as_ref()
    )
    .fetch_one(pg_pool)
    .await?;
    debug!("found record: {:?}", record);

    Ok(record.id)
}

pub async fn create_post(
    pg_pool: &sqlx::Pool<Postgres>,
    post: Post,
) -> std::result::Result<i32, sqlx::Error> {
    debug!("creating post in database: {:?}", post);
    let record = sqlx::query!(
        r#"
INSERT INTO POSTS ( TITLE, BODY, AUTHOR )
VALUES ( $1, $2, $3 )
RETURNING ID
        "#,
        post.title,
        post.body,
        post.author
    )
    .fetch_one(pg_pool)
    .await?;
    debug!("created post for {post:?} with id {}", record.id);

    Ok(record.id)
}

pub async fn read_post(
    pg_pool: &sqlx::Pool<Postgres>,
    post_id: PostId,
) -> std::result::Result<Post, sqlx::Error> {
    use std::str::FromStr;
    debug!("fetching post with id: {post_id:?} from database");
    let record = sqlx::query!(
        r#"
SELECT TITLE, BODY, AUTHOR, CREATED_AT
FROM POSTS
WHERE ID = $1
        "#,
        i32::from_str(&post_id.id).unwrap()
    )
    .fetch_one(pg_pool)
    .await?;
    debug!("found record: {record:?}");

    Ok(Post {
        title: record.title,
        body: record.body,
        author: record.author,
    })
}

pub async fn update_post(
    pg_pool: &sqlx::Pool<Postgres>,
    post_update: PostUpdate,
) -> std::result::Result<i32, sqlx::Error> {
    use std::str::FromStr;
    debug!("updating post: {post_update:?}");
    let post_id = i32::from_str(&post_update.post_id.unwrap().id).unwrap();
    let new_post = post_update.new_post.unwrap();
    let record = sqlx::query!(
        r#"
UPDATE POSTS 
SET TITLE=$2, BODY=$3, AUTHOR=$4
WHERE ID = $1
RETURNING ID
        "#,
        post_id,
        new_post.title,
        new_post.body,
        new_post.author
    )
    .fetch_one(pg_pool)
    .await?;
    debug!("updated post with id: {}", record.id);

    Ok(record.id)
}

pub async fn delete_post(
    pg_pool: &sqlx::Pool<Postgres>,
    post_id: PostId,
) -> std::result::Result<i32, sqlx::Error> {
    use std::str::FromStr;
    debug!("deleting post with id: {post_id:?}");
    let post_id = i32::from_str(&post_id.id).unwrap();
    let record = sqlx::query!(
        r#"
DELETE FROM POSTS
WHERE ID = $1
RETURNING ID
        "#,
        post_id
    )
    .fetch_one(pg_pool)
    .await?;
    debug!("deleted post with id: {}", record.id);

    Ok(record.id)
}

pub async fn all_posts(
    pg_pool: &sqlx::Pool<Postgres>,
    author: impl AsRef<str>,
) -> std::result::Result<Vec<i32>, sqlx::Error> {
    debug!("looking up all posts for author: {}", author.as_ref());
    let records = sqlx::query!(
        r#"
SELECT ID FROM POSTS
WHERE AUTHOR = $1
        "#,
        author.as_ref()
    )
    .fetch_all(pg_pool)
    .await?;

    let mut post_ids = vec![];
    for record in records {
        post_ids.push(record.id);
    }
    debug!("found posts with ids: {post_ids:?}");

    Ok(post_ids)
}

#[derive(Debug)]
pub struct User {
    pub id: i32,
    pub email: String,
    pub username: String,
    pub password: String,
    pub registered: PrimitiveDateTime,
}

pub async fn select_user_email(
    pg_pool: &sqlx::Pool<Postgres>,
    email: impl AsRef<str>,
) -> std::result::Result<Option<User>, sqlx::Error> {
    debug!("looking up user with email: {}", email.as_ref());
    let records = sqlx::query!(
        r#"
SELECT ID, EMAIL, USERNAME, PASSWORD, REGISTERED
FROM USERS 
WHERE EMAIL = $1
ORDER BY ID
        "#,
        email.as_ref()
    )
    .fetch_all(pg_pool)
    .await?;

    let mut user = None;
    for record in records {
        user = Some(User {
            id: record.id,
            email: record.email,
            username: record.username,
            password: record.password,
            registered: record.registered,
        });
        debug!("found user: {user:?}");
    }

    Ok(user)
}

pub async fn select_user_op_level(
    pg_pool: &sqlx::Pool<Postgres>,
    user_id: i32,
) -> std::result::Result<Option<OpLevel>, sqlx::Error> {
    debug!("looking up op level for user: {user_id}");
    let record = sqlx::query!(
        r#"
SELECT OP_LEVEL AS "op_level: OpLevel" FROM USERS
WHERE ID = $1
        "#,
        user_id
    ).fetch_one(pg_pool).await?;

    Ok(record.op_level)
}