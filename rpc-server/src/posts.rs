use sqlx::PgPool;
use std::sync::Arc;

pub struct PostService {
    app_key: String,
    pg_pool: Arc<PgPool>,
}

impl PostService {
    pub fn new(app_key: String, pg_pool: Arc<PgPool>) -> Self {
        Self { app_key, pg_pool }
    }
}

pub mod post_api {
    tonic::include_proto! {"post"}
}
use post_api::posts_server::Posts;
use post_api::{AllPostsRequest, AllPostsResponse, Post, PostId, PostUpdate, ReadPostResponse};
use tonic::{Request, Response, Status};
use tracing::{debug, trace, info};

use crate::auth::auth_api::AuthToken;
use crate::{auth, database, OpLevel};

#[tonic::async_trait]
impl Posts for PostService {
    #[tracing::instrument(skip(self))]
    async fn create_post(
        &self,
        request: Request<Post>,
    ) -> std::result::Result<Response<PostId>, tonic::Status> {
        info!("received create post request: {request:?}");
        let token = request
            .metadata()
            .get("x-authorization")
            .ok_or(Status::unauthenticated("No access token specified"))?
            .to_str()
            .map_err(|_| Status::unauthenticated("No access token specified"))?
            .to_string();
        let request = request.into_inner();

        debug!("verifying token: {token}");
        match auth::verify_token(
            &self.app_key,
            AuthToken {
                token: token.to_string(),
            },
        ) {
            Ok(user_id) => {
                let op_level = database::select_user_op_level(
                    &self.pg_pool, user_id
                ).await.map_err(|e| Status::unavailable(
                    format!("unable to retrieve authorization for user {user_id}: {}", e.to_string())
                ))?.ok_or(Status::unavailable("user has no authorization"))?;

                if OpLevel::Admin != op_level || OpLevel::Poster != op_level {
                    return Err(Status::unavailable("user does not have privileges to post"));
                }

                trace!("<authenticated>");
            },
            Err(_) => return Err(Status::unauthenticated("Invalid token")),
        }

        let post_id = database::create_post(
            &self.pg_pool,
            Post {
                author: request.author,
                title: request.title,
                body: request.body,
            },
        )
        .await
        .map_err(|e| Status::unavailable(format!("Unable to create post: {}", e.to_string())))?;

        Ok(Response::new(PostId {
            id: post_id.to_string(),
        }))
    }

    #[tracing::instrument(skip(self))]
    async fn read_post(
        &self,
        request: Request<PostId>,
    ) -> std::result::Result<Response<ReadPostResponse>, tonic::Status> {
        info!("received read post request: {request:?}");
        let request = request.into_inner();

        let post = database::read_post(&self.pg_pool, request)
            .await
            .map_err(|e| Status::unavailable(format!("Unable to fetch post: {}", e.to_string())))?;
        debug!("found post in database: {post:?}");

        Ok(Response::new(ReadPostResponse { post: Some(post) }))
    }

    async fn update_post(
        &self,
        request: Request<PostUpdate>,
    ) -> std::result::Result<Response<PostId>, tonic::Status> {
        info!("received update post request: {request:?}");
        let token = request
            .metadata()
            .get("x-authorization")
            .ok_or(Status::unauthenticated("No access token specified"))?
            .to_str()
            .map_err(|_| Status::unauthenticated("No access token specified"))?
            .to_string();
        let request = request.into_inner();

        debug!("verifying token: {token:?}");
        match auth::verify_token(
            &self.app_key,
            AuthToken {
                token: token.to_string(),
            },
        ) {
            Ok(user_id) => {
                let op_level = database::select_user_op_level(
                    &self.pg_pool, user_id
                ).await.map_err(|e| Status::unavailable(
                    format!("unable to retrieve authorization for user {user_id}: {}", e.to_string())
                ))?.ok_or(Status::unavailable("user has no authorization"))?;

                if OpLevel::Admin != op_level || OpLevel::Poster != op_level {
                    return Err(Status::unavailable(&format!("user does not have privileges to post: {op_level:?}")));
                }

                trace!("<authenticated>");
            },
            Err(_) => return Err(Status::unauthenticated("Invalid token")),
        }

        let post_id = database::update_post(&self.pg_pool, request)
            .await
            .map_err(|e| {
                Status::unavailable(format!("Unable to update post: {}", e.to_string()))
            })?;

        Ok(Response::new(PostId {
            id: post_id.to_string(),
        }))
    }

    async fn delete_post(
        &self,
        request: Request<PostId>,
    ) -> std::result::Result<Response<PostId>, tonic::Status> {
        info!("received delete post request: {request:?}");
        let token = request
            .metadata()
            .get("x-authorization")
            .ok_or(Status::unauthenticated("No access token specified"))?
            .to_str()
            .map_err(|_| Status::unauthenticated("No access token specified"))?
            .to_string();
        let request = request.into_inner();

        debug!("verifying token: {token:?}");
        match auth::verify_token(
            &self.app_key,
            AuthToken {
                token: token.to_string(),
            },
        ) {
            Ok(user_id) => {
                let op_level = database::select_user_op_level(
                    &self.pg_pool, user_id
                ).await.map_err(|e| Status::unavailable(
                    format!("unable to retrieve authorization for user {user_id}: {}", e.to_string())
                ))?.ok_or(Status::unavailable("user has no authorization"))?;

                if OpLevel::Admin != op_level {
                    return Err(Status::unavailable("user does not have privileges to post"));
                }

                trace!("<authenticated>");
            },
            Err(_) => return Err(Status::unauthenticated("Invalid token")),
        }

        let post_id = database::delete_post(&self.pg_pool, request)
            .await
            .map_err(|e| {
                Status::unavailable(format!("Unable to delete post: {}", e.to_string()))
            })?;

        Ok(Response::new(PostId {
            id: post_id.to_string(),
        }))
    }

    async fn all_posts(
        &self,
        request: Request<AllPostsRequest>,
    ) -> std::result::Result<Response<AllPostsResponse>, tonic::Status> {
        info!("received all posts request: {request:?}");
        let request = request.into_inner();
        let post_ids: Vec<PostId> = database::all_posts(&self.pg_pool, request.author)
            .await
            .map_err(|e| {
                Status::unavailable(format!("Unable to fetch all post ids: {}", e.to_string()))
            })?
            .iter()
            .map(|id| PostId { id: id.to_string() })
            .collect();

        Ok(Response::new(AllPostsResponse {
            all_posts: post_ids,
        }))
    }
}
