use clap::Parser;
use tonic::transport::Server;

use jenr_blog::auth::{auth_api::auth_server::AuthServer, AuthService};
use jenr_blog::posts::post_api::posts_server::PostsServer;
use jenr_blog::{database, posts};
use tracing::{trace, info};

#[tokio::main]
#[tracing::instrument]
async fn main() -> jenr_blog::Result<()> {
    let subscriber = tracing_subscriber::FmtSubscriber::new();
    tracing::subscriber::set_global_default(subscriber)?;

    trace!("parsing command line args...");
    let args = jenr_blog::Args::parse();

    trace!("connecting to postgres database...");
    let pg_pool = database::connect(&args).await?;

    let addr = format!("{}:{}", args.app_host, args.app_port).parse()?;

    info!("starting gRPC server at {:?}", addr);
    Server::builder()
        .add_service(AuthServer::new(AuthService::new(
            args.app_key.clone(),
            pg_pool.clone(),
        )))
        .add_service(PostsServer::new(posts::PostService::new(
            args.app_key.clone(),
            pg_pool.clone(),
        )))
        .serve(addr)
        .await?;

    Ok(())
}
