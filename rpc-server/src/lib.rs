pub mod auth;
pub mod database;
pub mod posts;

pub const DEFAULT_EXP: u32 = 3600;
pub const MAX_CONNECTIONS: u32 = 5;

use clap::Parser;
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
pub struct Args {
    #[arg(long)]
    pg_host: String,
    #[arg(long)]
    pg_port: u64,
    #[arg(long)]
    pg_user: String,
    #[arg(long)]
    pg_password: String,
    #[arg(long)]
    pub app_key: String,
    #[arg(long, default_value="0.0.0.0")]
    pub app_host: String,
    #[arg(long, default_value_t=50051)]
    pub app_port: usize,
}

use thiserror::Error;
#[derive(Error, Debug)]
pub enum Error {
    #[error("database error")]
    Database(#[from] sqlx::Error),
    #[error("rpc server error")]
    RpcServer(#[from] tonic::transport::Error),
    #[error("address parse error")]
    AddrParseError(#[from] std::net::AddrParseError),
    #[error("tracing subscriber error")]
    SetGlobalDefaultError(#[from] tracing::subscriber::SetGlobalDefaultError)
}
pub type Result<T> = std::result::Result<T, Error>;

#[derive(Clone, Debug, PartialEq, PartialOrd, sqlx::Type)]
#[sqlx(type_name = "op_level", rename_all = "lowercase")]
pub enum OpLevel {
    Poster,
    Commenter,
    Admin
}