use crate::database;

#[derive(Debug)]
pub struct AuthService {
    app_key: String,
    pg_pool: Arc<PgPool>,
}

impl AuthService {
    pub fn new(app_key: String, pg_pool: Arc<PgPool>) -> Self {
        Self { app_key, pg_pool }
    }
}

pub mod auth_api {
    tonic::include_proto! {"auth"}
}
use auth_api::auth_server::Auth;
use auth_api::{AuthToken, LoginRequest, RegisterRequest};
use bcrypt::verify;
use hmac::Hmac;
use jwt::{SignWithKey, VerifyWithKey};
use sqlx::PgPool;
use thiserror::Error;
use tracing::{info, trace, debug};
use std::collections::{BTreeMap, HashMap};
use std::sync::Arc;
use std::time::{SystemTime, UNIX_EPOCH};
use tonic::{Request, Response, Status};

#[tonic::async_trait]
impl Auth for AuthService {
    #[tracing::instrument(skip(self))]
    async fn login(
        &self,
        request: Request<LoginRequest>,
    ) -> std::result::Result<Response<AuthToken>, tonic::Status> {
        info!("received login request: {request:?}");
        let request = request.into_inner();
        let user = database::select_user_email(&self.pg_pool, request.email)
            .await
            .map_err(|e| {
                Status::unavailable(format!("Error while looking up user: {}", e.to_string()))
            })?
            .ok_or(Status::unauthenticated("Invalid user"))?;

        match verify(&request.password, &user.password) {
            Ok(true) => trace!("<authenticated>"),
            Ok(false) | Err(_) => return Err(Status::unauthenticated("Invalid password")),
        }

        let auth_token = generate_token(&self.app_key, user.id)
            .map_err(|_| Status::unauthenticated("Unable to generate auth token"))?;
        debug!("generated token: {auth_token:?}");

        Ok(Response::new(auth_token))
    }

    #[tracing::instrument(skip(self))]
    async fn register(
        &self,
        request: Request<RegisterRequest>,
    ) -> std::result::Result<Response<AuthToken>, tonic::Status> {
        info!("received register request: {request:?}");
        let request = request.into_inner();
        let password_hash = bcrypt::hash(&request.password, 10).map_err(|e| {
            Status::unavailable(format!("Error while hashing password: {}", e.to_string()))
        })?;

        let user_id = database::create_user(
            &self.pg_pool,
            &request.email,
            &password_hash,
            &request.user_name,
        )
        .await
        .map_err(|e| Status::unavailable(format!("Unable to create user: {}", e.to_string())))?;
        debug!("created user with id: {user_id}");

        let auth_token = generate_token(&self.app_key, user_id)
            .map_err(|_| Status::unavailable("Unable to generate auth token"))?;
        trace!("created auth token: {auth_token:?}");

        Ok(Response::new(auth_token))
    }
}

pub struct GenerateTokenError;

pub struct GenerateClaimsError;
use crate::DEFAULT_EXP;
fn generate_claims(user_id: i32) -> Result<BTreeMap<&'static str, String>, GenerateClaimsError> {
    let mut claims: BTreeMap<&str, String> = BTreeMap::new();

    claims.insert("sub", user_id.to_string());

    let current_timestamp = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .map_err(|_| GenerateClaimsError)?
        .as_secs();

    claims.insert("iat", current_timestamp.to_string());
    claims.insert("exp", String::from(DEFAULT_EXP.to_string()));

    Ok(claims)
}

use hmac::digest::{KeyInit, InvalidLength};
use sha2::Sha256;
fn generate_token(app_key: &str, user_id: i32) -> Result<AuthToken, GenerateTokenError> {
    let key: Hmac<Sha256> =
        Hmac::new_from_slice(app_key.as_bytes()).map_err(|_| GenerateTokenError)?;

    let claims = generate_claims(user_id).map_err(|_| GenerateTokenError)?;

    let token = claims.sign_with_key(&key).map_err(|_| GenerateTokenError)?;

    Ok(AuthToken { token })
}

#[derive(Debug, Error)]
pub enum VerifyTokenError {
    #[error("token verification error")]
    VerificationError(#[from] jwt::Error),
    #[error("key length error")]
    KeyLengthError(#[from] InvalidLength),
    #[error("claims error")]
    ClaimsError(String)
}

pub fn verify_token(app_key: impl AsRef<str>, token: AuthToken) -> Result<i32, VerifyTokenError> {
    let key: Hmac<Sha256> =
        Hmac::new_from_slice(app_key.as_ref().as_bytes())?;
    let claims: HashMap<String, String> = token
        .token.verify_with_key(&key)?;
    let sub = claims.get("sub")
        .ok_or(VerifyTokenError::ClaimsError(String::from("no sub claim")))?;
    let user_id = str::parse::<i32>(sub)
        .map_err(|e| VerifyTokenError::ClaimsError(
            format!("unable to parse sub claim: {}", e.to_string())
        ))?;
    Ok(user_id)
}
