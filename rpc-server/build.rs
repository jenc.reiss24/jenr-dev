fn main() -> Result<(), Box<dyn std::error::Error>> {
    println!("cargo:rerun-if-changed=migrations");
    println!("cargo:rerun-if-changed=proto");
    tonic_build::configure().build_server(true).compile(
        &["proto/blog/post.proto", "proto/blog/auth.proto"],
        &["proto/blog"],
    )?;
    Ok(())
}