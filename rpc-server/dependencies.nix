{nixpkgs ? <nixpkgs>}: rec {
  oxalica =
    import (builtins.fetchTarball
      "https://github.com/oxalica/rust-overlay/archive/master.tar.gz");

  pkgs = import nixpkgs {
    overlays = [oxalica];
  };

  rust =
    pkgs.rust-bin.fromRustupToolchainFile
    ./rust-toolchain.toml;

  rust-platform = pkgs.makeRustPlatform {
    rustc = rust;
    cargo = rust;
  };

  runtimeDependencies = with pkgs; [
    openssl
  ];

  frameworks = pkgs.darwin.apple_sdk.frameworks;

  buildDependencies = with pkgs;
    [
      llvmPackages.libclang
      clang
      pkg-config
      protoc-gen-doc 
      protoc-gen-rust 
      protobuf
    ]
    ++ lib.optionals stdenv.isDarwin
    [
      frameworks.Security
      frameworks.CoreServices
    ]
    ++ runtimeDependencies;

  developmentDependencies = with pkgs;
    [
      rust
      rust-analyzer
      rnix-lsp
      alejandra
      cargo-nextest
      cargo-audit
      cargo-machete
      sqlx-cli
      grpcurl
    ]
    ++ buildDependencies;
}
