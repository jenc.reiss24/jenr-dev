let
  dependencies =
    import
    ./dependencies.nix {};

  cargo-toml =
    builtins.fromTOML
    (builtins.readFile ./Cargo.toml);

  LIBCLANG_PATH = "${dependencies.pkgs.llvmPackages.libclang.lib}/lib";
  BINDGEN_EXTRA_CLANG_ARGS =
    if dependencies.pkgs.stdenv.isDarwin
    then "-isystem ${dependencies.pkgs.stdenv.cc.cc}/lib/clang/${dependencies.pkgs.lib.getVersion dependencies.pkgs.stdenv.cc.cc}/include"
    else "-isystem ${dependencies.pkgs.llvmPackages.libclang.lib}/lib/clang/${dependencies.pkgs.lib.getVersion dependencies.pkgs.clang}/include";
in
  dependencies.rust-platform.buildRustPackage rec {
    pname = cargo-toml.package.name;
    version = cargo-toml.package.version;

    srcs = [ ./. ../proto/. ../migrations/. ];
    
    sourceRoot = "./rpc-server";
    postUnpack = ''
      cp -r proto      rpc-server/
      cp -r migrations rpc-server/
    '';

    cargoLock = {
      lockFile = ./Cargo.lock;
    };

    nativeBuildInputs = dependencies.buildDependencies;
    buildInputs = dependencies.runtimeDependencies;

    preBuild = ''
      export LIBCLANG_PATH="${LIBCLANG_PATH}"
      export BINDGEN_EXTRA_CLANG_ARGS="${BINDGEN_EXTRA_CLANG_ARGS}"

    '';
    doCheck = false;
  }
