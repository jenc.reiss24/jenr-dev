FROM postgres:latest
FROM nixos/nix:latest

RUN nix-channel --update

ARG PG_HOST="localhost"
ARG PG_USER="postgres"
ARG PG_PASSWORD="postgres"
ARG PG_PORT=5432

ENV PG_HOST ${PG_HOST}
ENV PG_USER ${PG_USER}
ENV PG_PASSWORD ${PG_PASSWORD}
ENV PG_PORT ${PG_PORT}

COPY ./run-migrations.sh /docker-entrypoint-initdb.d/